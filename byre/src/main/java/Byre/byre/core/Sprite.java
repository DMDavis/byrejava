package Byre.byre.core;

import java.awt.image.BufferedImage;

public class Sprite {

	public int xpos, ypos;
	public byte palette;
	public boolean flipx, flipy;
	public byte[] pixs = new byte[32];

	// rasterize a sprite onto an image.

	public void drawSprite(BufferedImage target, Palette p) {
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (j + ypos >= 0 && j + ypos < 256 && i + xpos >= 0
						&& i + xpos < 256) {
					int n = (j * 8 + i) / 2;

					int k = (i % 2 == 0)
							? (pixs[n] & 0xF0) >> 4
							: pixs[n] & 0x0F;

					target.setRGB(i + xpos, j + ypos,
							p.getColor(palette, (byte) k));
				}
			}
		}
	}
}
