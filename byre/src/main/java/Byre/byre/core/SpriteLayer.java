package Byre.byre.core;

import java.awt.Graphics;

public class SpriteLayer extends Layer {

	public Sprite[] sprites = new Sprite[0];
	Palette p;

	public SpriteLayer(Palette pal) {
		p = pal;
	}

	// called every frame to draw graphics.
	@Override
	public void draw(Graphics g) {
		// clear layer
		for (int i = 0; i < bi.getWidth(); i++) {
			for (int j = 0; j < bi.getHeight(); j++) {
				bi.setRGB(i, j, 0x00000000);
			}
		}
		// write sprites to layer
		for (int i = 0; i < sprites.length; i++) {
			if (sprites[i] != null && sprites[i].xpos > -16
					&& sprites[i].xpos < 256 && sprites[i].ypos > -16
					&& sprites[i].ypos < 256) {
				sprites[i].drawSprite(bi, p);
			}
		}
		super.draw(g);
	}
}
